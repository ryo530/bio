// main.js

// =========================================
// 即時実行
// =========================================
$(function() {
	// -----------------------------------------
	// イベント登録
	// -----------------------------------------
	// リンク；GitLab
	$('#linkGitlab').on('click', function() {
		window.open('https://gitlab.com/ryo530', '_blank')
	});
	// リンク；Code Note
	$('#linkCodenote').on('click', function() {
		window.open('https://hackmd.io/@ryo/codenote', '_blank')
	});
	// リンク；Qiita
	$('#linkQiita').on('click', function() {
		window.open('https://qiita.com/ryo_____', '_blank')
	});
	// リンク；Code Trial 1
	$('#linkCodetrial1').on('click', function() {
		window.open('https://ryo530.gitlab.io/play-room/', '_blank')
	});
	// リンク；Code Trial 1
	$('#linkCodetrial2').on('click', function() {
		window.open('https://ryo530.gitlab.io/firstreact/', '_blank')
	});
	
})